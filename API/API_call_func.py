def get_ai_response(prompt):
    # API call to generate a response from the language model
    try:
        response = openai.Completion.create(
            engine="text-davinci-003",  # Specify the model you are using
            prompt=prompt,
            max_tokens=50,  # Limit the response length as needed
            n=1,
            stop=None,
            temperature=0.7  # Controls creativity of response
        )
        return response.choices[0].text.strip()
    except Exception as e:
        print("Error calling the language model API:", e)
        return "Error generating response."
