Bir bahçemiz var; bir taraf çiçekli, bir tarafsa çöl
Bir tarafta gökkuşağı, öbür tarafsa kör
Sınırda kalmışlardanız biz, hep sınıfta kalmışlardan
Çok uzaktayız, sıkıntı çekmişlere yakın bir yerde
Çölde kazanılan zaferler hepsi kanla yazılır
Ahmak olmasaydın insan, tüm zaferler dostça kazanılırdı
Her gün doğumundan gün batımına
Her geceden gündüze işlenen bir suç var
Her bi' yerde bahçemiz var
Cümle derde ol deva diye dua ederdi günde bin defa
Fayda yok, bu çok fena, çare yok
Bu bir bela, sanki yoktu başta
Hepsi kalsın aleminde Sagopa ve Ceza, Rap için bir pranga
Akarsular bu bahçelerde, kurtulur zebanilerden
Akmayan suyuyla çölde, çeşmeler var her bi' yerde, bul
Olmaya çalış bi' kul
İstediğimiz yekti sulh
Olmasın altında çul
Olmasın paran ve pul
Gene de gül bir kez be, bi' kere gül
Ve senede bir de olsa gül bu çölde yeşerir elbet
Savaş biter ve biz de sınırın ortasında kaybolur gider' de
Sözlerimizi ve Rap'imizi miras bırakırız yeter

source: https://genius.com/Ceza-neyim-var-ki-lyrics
