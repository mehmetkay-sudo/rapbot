# created some classes to use later for rapbot / rhyme schemes
class rhymes:
    rhyme01 = (str("cry ") + str("try ") + str("fly "))
    rhyme02 = (str("sing ") + str("bring ") + str("swing "))
    rhyme03 = (str("fill ") + str("bill ") + str("drill "))
    
class rhymes2:
    rhyme04 = (str("yawn ") + str("frown ") + str("drawn "))
    rhyme05 = (str("breath ") + str("craft ") + str("theft "))
    rhyme06 = (str("feel ") + str("real ") + str("deal "))

rhyme1 = rhymes()
print(rhyme1.rhyme01)
print(rhyme1.rhyme02)
print(rhyme1.rhyme03)

rhyme2 = rhymes2()
print(rhyme2.rhyme04)
print(rhyme2.rhyme05)
print(rhyme2.rhyme06)
